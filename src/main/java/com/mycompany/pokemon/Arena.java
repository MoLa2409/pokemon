package com.mycompany.pokemon;

import java.util.Scanner;

public class Arena {

    Feuer Charmander = new Charmander();
    Wasser Squirtle = new Squirtle();
    Pflanze Bulbasaur = new Bulbasaur();

    Scanner scanner = new Scanner(System.in);

    Spieler Spieler1 = new Spieler();
    Spieler Spieler2 = new Spieler();

    Kampf ArenaKampf = new Kampf();

    public void spielerDaten() {

        System.out.println("Geben Sie den Namen von Spieler1 an:");
        String nameSpieler1 = scanner.next();
        Spieler1.setSpielerName(nameSpieler1);
        System.out.println("Willkommen in der Arena " + Spieler1.getSpielerName());
        System.out.println();

        System.out.println("Geben Sie den Namen von Spieler2 an:");
        String nameSpieler2 = scanner.next();
        Spieler2.setSpielerName(nameSpieler2);
        System.out.println("Willkommen in der Arena " + Spieler2.getSpielerName());
        System.out.println();

        System.out.println("Wählen Sie ihr Pokemon " + Spieler1.getSpielerName());
        System.out.println();
        System.out.println("1 - Charmender");
        System.out.println("2 - Squirtle");
        System.out.println("3 - Bulbasaur");

        int eingabe = scanner.nextInt();

        switch (eingabe) {
            case 1:
                Spieler1.setSpielerFeuerPokemon(Charmander);
                System.out.println(Spieler1.getSpielerName() + " hat Charmender gewählt");
                System.out.println();
                break;
            case 2:
                Spieler1.setSpielerWasserPokemon(Squirtle);
                System.out.println(Spieler1.getSpielerName() + " hat Squirtle gewählt");
                System.out.println();
                break;
            case 3:
                Spieler1.setSpielerPflanzePokemon(Bulbasaur);
                System.out.println(Spieler1.getSpielerName() + " hat Bulbasaur gewählt");
                System.out.println();
                break;
            default:
                break;
        }

        System.out.println("Wählen Sie ihr Pokemon " + Spieler2.getSpielerName());
        System.out.println();
        System.out.println("1 - Charmender");
        System.out.println("2 - Squirtle");
        System.out.println("3 - Bulbasaur");

        int eingabe2 = scanner.nextInt();

        switch (eingabe2) {
            case 1:
                Spieler2.setSpielerFeuerPokemon(Charmander);
                System.out.println(Spieler2.getSpielerName() + " hat Charmender gewählt");
                System.out.println();
                break;
            case 2:
                Spieler2.setSpielerWasserPokemon(Squirtle);
                System.out.println(Spieler2.getSpielerName() + " hat Squirtle gewählt");
                System.out.println();
                break;
            case 3:
                Spieler2.setSpielerPflanzePokemon(Bulbasaur);
                System.out.println(Spieler2.getSpielerName() + " hat Bulbasaur gewählt");
                System.out.println();
                break;
            default:
                break;
        }
    }

    public void kampf() {
        if (Spieler1.getSpielerFeuerPokemon() != null && Spieler2.getSpielerFeuerPokemon() != null) {
            ArenaKampf.FeuerGegenFeuer(Spieler1, Spieler2);

        } else if (Spieler1.getSpielerWasserPokemon() != null && Spieler2.getSpielerWasserPokemon() != null) {
            ArenaKampf.WasserGegenWasser(Spieler1, Spieler2);

        } else if (Spieler1.getSpielerPflanzePokemon() != null && Spieler2.getSpielerPflanzePokemon() != null) {
            ArenaKampf.PflanzeGegenPflanze(Spieler1, Spieler2);

        } else if (Spieler1.getSpielerWasserPokemon() != null && Spieler2.getSpielerFeuerPokemon() != null) {  //  WASSER VS FEUER
            ArenaKampf.WasserGegenFeuer(Spieler1, Spieler2);

        } else if (Spieler1.getSpielerFeuerPokemon() != null && Spieler2.getSpielerWasserPokemon() != null) {  // FEUER VS WASSER
            ArenaKampf.FeuerGegenWasser(Spieler1, Spieler2);

        } else if (Spieler1.getSpielerFeuerPokemon() != null && Spieler2.getSpielerPflanzePokemon() != null) {  //  FEUER VS PFLANZE
            ArenaKampf.FeuerGegenPflanze(Spieler1, Spieler2);

        } else if (Spieler1.getSpielerPflanzePokemon() != null && Spieler2.getSpielerFeuerPokemon() != null) {  // PFLANZE VS FEUER
            ArenaKampf.PflanzeGegenFeuer(Spieler1, Spieler2);

        } else if (Spieler1.getSpielerPflanzePokemon() != null && Spieler2.getSpielerWasserPokemon() != null) {  // PFLANZE VS WASSER
            ArenaKampf.PflanzeGegenWasser(Spieler1, Spieler2);

        } else if (Spieler1.getSpielerWasserPokemon() != null && Spieler2.getSpielerPflanzePokemon() != null) {  // WASSER VS PFLANZE
            ArenaKampf.WasserGegenPflanze(Spieler1, Spieler2);
        }

    }
}
