package com.mycompany.pokemon;

public class Spieler {

    private String name;
    Feuer spielerFeuerPokemon;
    Wasser spielerWasserPokemon;
    Pflanze spielerPflanzePokemon;

    public Spieler() {
        
    }

    public String getSpielerName() {
        return name;
    }

    public void setSpielerName(String name) {
        this.name = name;
    }

    public Feuer getSpielerFeuerPokemon() {
        return spielerFeuerPokemon;
    }
 
    public void setSpielerFeuerPokemon(Feuer spielerFeuerPokemon) {
        this.spielerFeuerPokemon = spielerFeuerPokemon;
    }

    public Wasser getSpielerWasserPokemon() {
        return spielerWasserPokemon;
    }

    public void setSpielerWasserPokemon(Wasser spielerWasserPokemon) {
        this.spielerWasserPokemon = spielerWasserPokemon;
    }

    public Pflanze getSpielerPflanzePokemon() {
        return spielerPflanzePokemon;
    }

    public void setSpielerPflanzePokemon(Pflanze spielerPflanzePokemon) {
        this.spielerPflanzePokemon = spielerPflanzePokemon;
    }

    
}
