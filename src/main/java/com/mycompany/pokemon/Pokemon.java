package com.mycompany.pokemon;

public class Pokemon {

    private int attackPunkte;
    private int spezialAttacke;
    private int lebensPunkte;

    public Pokemon(int aP, int sA, int lP) {
        this.attackPunkte = aP;
        this.spezialAttacke = sA;
        this.lebensPunkte = lP;
    }

    public int getAttackPunkte() {
        return attackPunkte;
    }

    public void setAttackPunkte(int attackPunkte) {
        this.attackPunkte = attackPunkte;
    }
    
    public int getSpezialAttacke() {
        return spezialAttacke;
    }

    public void setSpezialAttacke(int spezialAttacke) {
        this.spezialAttacke = spezialAttacke;
    }
    
    public int getLebensPunkte() {
        return lebensPunkte;
    }

    public void setLebensPunkte(int lebensPunkte) {
        this.lebensPunkte = lebensPunkte;
    }
    
    
    

}
