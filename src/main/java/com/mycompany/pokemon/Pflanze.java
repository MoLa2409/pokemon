package com.mycompany.pokemon;

public class Pflanze extends Pokemon {

    private String name;
    private String Element;

    public Pflanze(String n, String e, int aP, int sA, int lP) {
        super(aP, sA, lP);
        this.name = n;
        this.Element = e;
    }
}
