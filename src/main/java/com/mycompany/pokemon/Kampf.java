package com.mycompany.pokemon;

import java.util.Scanner;

public class Kampf {

    Scanner scanner = new Scanner(System.in);

    public void FeuerGegenFeuer(Spieler Spieler1, Spieler Spieler2) {

        System.out.println("FEUER GEGEN FEUER");
        System.out.println();

        System.out.println(Spieler1.getSpielerName() + " ist an der Reihe!");
        System.out.println("1 - Standardattacke");
        System.out.println("2 - Spezialattacke");
        int eingabe = scanner.nextInt();

        switch (eingabe) {
            case 1 -> {
                Spieler2.getSpielerFeuerPokemon().setLebensPunkte(Spieler2.getSpielerFeuerPokemon().getLebensPunkte() - Spieler1.getSpielerFeuerPokemon().getAttackPunkte());
                System.out.println(Spieler2.getSpielerName() + " HP: " + Spieler2.getSpielerFeuerPokemon().getLebensPunkte());
            }
            case 2 -> {
                Spieler2.getSpielerFeuerPokemon().setLebensPunkte(Spieler2.getSpielerFeuerPokemon().getLebensPunkte() - Spieler1.getSpielerFeuerPokemon().getSpezialAttacke());
                System.out.println(Spieler2.getSpielerName() + " HP: " + Spieler2.getSpielerFeuerPokemon().getLebensPunkte());
            }
            default -> {
            }
        }
        System.out.println(Spieler2.getSpielerName() + " ist an der Reihe!");
        System.out.println("1 - Standardattacke");
        System.out.println("2 - Spezialattacke");
        int eingabe2 = scanner.nextInt();

        switch (eingabe2) {
            case 1 -> {
                Spieler1.getSpielerFeuerPokemon().setLebensPunkte(Spieler1.getSpielerFeuerPokemon().getLebensPunkte() - Spieler2.getSpielerFeuerPokemon().getAttackPunkte());
                System.out.println(Spieler1.getSpielerName() + " HP: " + Spieler1.getSpielerFeuerPokemon().getLebensPunkte());
            }
            case 2 -> {
                Spieler1.getSpielerFeuerPokemon().setLebensPunkte(Spieler1.getSpielerFeuerPokemon().getLebensPunkte() - Spieler2.getSpielerFeuerPokemon().getSpezialAttacke());
                System.out.println(Spieler1.getSpielerName() + " HP: " + Spieler1.getSpielerFeuerPokemon().getLebensPunkte());
            }
            default -> {
            }
        }
    }

    public void WasserGegenWasser(Spieler Spieler1, Spieler Spieler2) {
        System.out.println("WASSER GEGEN WASSER");
        System.out.println();

        System.out.println(Spieler1.getSpielerName() + " ist an der Reihe!");
        System.out.println("1 - Standardattacke");
        System.out.println("2 - Spezialattacke");
        int eingabe3 = scanner.nextInt();

        switch (eingabe3) {
            case 1 -> {
                Spieler2.getSpielerWasserPokemon().setLebensPunkte(Spieler2.getSpielerWasserPokemon().getLebensPunkte() - Spieler1.getSpielerWasserPokemon().getAttackPunkte());
                System.out.println(Spieler2.getSpielerName() + " HP: " + Spieler2.getSpielerWasserPokemon().getLebensPunkte());
            }
            case 2 -> {
                Spieler2.getSpielerWasserPokemon().setLebensPunkte(Spieler2.getSpielerWasserPokemon().getLebensPunkte() - Spieler1.getSpielerWasserPokemon().getSpezialAttacke());
                System.out.println(Spieler2.getSpielerName() + " HP: " + Spieler2.getSpielerWasserPokemon().getLebensPunkte());
            }
        }

        System.out.println(Spieler2.getSpielerName() + " ist an der Reihe!");
        System.out.println("1 - Standardattacke");
        System.out.println("2 - Spezialattacke");
        int eingabe4 = scanner.nextInt();

        switch (eingabe4) {
            case 1 -> {
                Spieler1.getSpielerWasserPokemon().setLebensPunkte(Spieler1.getSpielerWasserPokemon().getLebensPunkte() - Spieler2.getSpielerWasserPokemon().getAttackPunkte());
                System.out.println(Spieler1.getSpielerName() + " HP: " + Spieler1.getSpielerWasserPokemon().getLebensPunkte());
            }
            case 2 -> {
                Spieler1.getSpielerWasserPokemon().setLebensPunkte(Spieler1.getSpielerWasserPokemon().getLebensPunkte() - Spieler2.getSpielerWasserPokemon().getSpezialAttacke());
                System.out.println(Spieler1.getSpielerName() + " HP: " + Spieler1.getSpielerWasserPokemon().getLebensPunkte());
            }
        }
    }

    public void PflanzeGegenPflanze(Spieler Spieler1, Spieler Spieler2) {
        System.out.println(Spieler1.getSpielerName() + " ist an der Reihe!");
        System.out.println("1 - Standardattacke");
        System.out.println("2 - Spezialattacke");
        int eingabe5 = scanner.nextInt();

        switch (eingabe5) {
            case 1 -> {
                Spieler2.getSpielerPflanzePokemon().setLebensPunkte(Spieler2.getSpielerPflanzePokemon().getLebensPunkte() - Spieler1.getSpielerPflanzePokemon().getAttackPunkte());
                System.out.println(Spieler2.getSpielerName() + " HP: " + Spieler2.getSpielerPflanzePokemon().getLebensPunkte());
            }
            case 2 -> {
                Spieler2.getSpielerPflanzePokemon().setLebensPunkte(Spieler2.getSpielerPflanzePokemon().getLebensPunkte() - Spieler1.getSpielerPflanzePokemon().getSpezialAttacke());
                System.out.println(Spieler2.getSpielerName() + " HP: " + Spieler2.getSpielerPflanzePokemon().getLebensPunkte());
            }
        }

        System.out.println(Spieler2.getSpielerName() + " ist an der Reihe!");
        System.out.println("1 - Standardattacke");
        System.out.println("2 - Spezialattacke");
        int eingabe6 = scanner.nextInt();

        switch (eingabe6) {
            case 1 -> {
                Spieler1.getSpielerPflanzePokemon().setLebensPunkte(Spieler1.getSpielerPflanzePokemon().getLebensPunkte() - Spieler2.getSpielerPflanzePokemon().getAttackPunkte());
                System.out.println(Spieler1.getSpielerName() + " HP: " + Spieler1.getSpielerPflanzePokemon().getLebensPunkte());
            }
            case 2 -> {
                Spieler1.getSpielerPflanzePokemon().setLebensPunkte(Spieler1.getSpielerPflanzePokemon().getLebensPunkte() - Spieler2.getSpielerPflanzePokemon().getSpezialAttacke());
                System.out.println(Spieler1.getSpielerName() + " HP: " + Spieler1.getSpielerPflanzePokemon().getLebensPunkte());
            }
        }
    }

    public void WasserGegenFeuer(Spieler Spieler1, Spieler Spieler2) {
        System.out.println(Spieler1.getSpielerName() + " ist an der Reihe!");
        System.out.println("1 - Standardattacke");
        System.out.println("2 - Spezialattacke");
        int eingabe7 = scanner.nextInt();

        switch (eingabe7) {
            case 1:
                Spieler2.getSpielerFeuerPokemon().setLebensPunkte(Spieler2.getSpielerFeuerPokemon().getLebensPunkte() - Spieler1.getSpielerWasserPokemon().getAttackPunkte() * 2);
                System.out.println(Spieler2.getSpielerName() + " HP: " + Spieler2.getSpielerFeuerPokemon().getLebensPunkte());
                break;
            case 2:
                Spieler2.getSpielerFeuerPokemon().setLebensPunkte(Spieler2.getSpielerFeuerPokemon().getLebensPunkte() - Spieler1.getSpielerWasserPokemon().getSpezialAttacke() * 2);
                System.out.println(Spieler2.getSpielerName() + " HP: " + Spieler2.getSpielerFeuerPokemon().getLebensPunkte());
                break;
        }

        System.out.println(Spieler2.getSpielerName() + " ist an der Reihe!");
        System.out.println("1 - Standardattacke");
        System.out.println("2 - Spezialattacke");
        int eingabe8 = scanner.nextInt();

        switch (eingabe8) {
            case 1:
                Spieler1.getSpielerWasserPokemon().setLebensPunkte(Spieler1.getSpielerWasserPokemon().getLebensPunkte() - Spieler2.getSpielerFeuerPokemon().getAttackPunkte());
                System.out.println(Spieler1.getSpielerName() + " HP: " + Spieler1.getSpielerWasserPokemon().getLebensPunkte());
                break;
            case 2:
                Spieler1.getSpielerWasserPokemon().setLebensPunkte(Spieler1.getSpielerWasserPokemon().getLebensPunkte() - Spieler2.getSpielerFeuerPokemon().getSpezialAttacke());
                System.out.println(Spieler1.getSpielerName() + " HP: " + Spieler1.getSpielerWasserPokemon().getLebensPunkte());
                break;
        }
    }

    public void FeuerGegenWasser(Spieler Spieler1, Spieler Spieler2) {
        System.out.println(Spieler1.getSpielerName() + " ist an der Reihe!");
        System.out.println("1 - Standardattacke");
        System.out.println("2 - Spezialattacke");
        int eingabe7 = scanner.nextInt();

        switch (eingabe7) {
            case 1:
                Spieler2.getSpielerWasserPokemon().setLebensPunkte(Spieler2.getSpielerWasserPokemon().getLebensPunkte() - Spieler1.getSpielerFeuerPokemon().getAttackPunkte());
                System.out.println(Spieler2.getSpielerName() + " HP: " + Spieler2.getSpielerWasserPokemon().getLebensPunkte());
                break;
            case 2:
                Spieler2.getSpielerWasserPokemon().setLebensPunkte(Spieler2.getSpielerWasserPokemon().getLebensPunkte() - Spieler1.getSpielerFeuerPokemon().getSpezialAttacke());
                System.out.println(Spieler2.getSpielerName() + " HP: " + Spieler2.getSpielerWasserPokemon().getLebensPunkte());
                break;
        }

        System.out.println(Spieler2.getSpielerName() + " ist an der Reihe!");
        System.out.println("1 - Standardattacke");
        System.out.println("2 - Spezialattacke");
        int eingabe8 = scanner.nextInt();

        switch (eingabe8) {
            case 1:
                Spieler1.getSpielerFeuerPokemon().setLebensPunkte(Spieler1.getSpielerFeuerPokemon().getLebensPunkte() - Spieler2.getSpielerWasserPokemon().getAttackPunkte() * 2);
                System.out.println(Spieler1.getSpielerName() + " HP: " + Spieler1.getSpielerWasserPokemon().getLebensPunkte());
                break;
            case 2:
                Spieler1.getSpielerFeuerPokemon().setLebensPunkte(Spieler1.getSpielerFeuerPokemon().getLebensPunkte() - Spieler2.getSpielerWasserPokemon().getSpezialAttacke() * 2);
                System.out.println(Spieler1.getSpielerName() + " HP: " + Spieler1.getSpielerWasserPokemon().getLebensPunkte());
                break;
        }
    }

    public void FeuerGegenPflanze(Spieler Spieler1, Spieler Spieler2) {
        System.out.println(Spieler1.getSpielerName() + " ist an der Reihe!");
        System.out.println("1 - Standardattacke");
        System.out.println("2 - Spezialattacke");
        int eingabe7 = scanner.nextInt();

        switch (eingabe7) {
            case 1:
                Spieler2.getSpielerPflanzePokemon().setLebensPunkte(Spieler2.getSpielerPflanzePokemon().getLebensPunkte() - Spieler1.getSpielerFeuerPokemon().getAttackPunkte() * 2);
                System.out.println(Spieler2.getSpielerName() + " HP: " + Spieler2.getSpielerPflanzePokemon().getLebensPunkte());
                break;
            case 2:
                Spieler2.getSpielerPflanzePokemon().setLebensPunkte(Spieler2.getSpielerPflanzePokemon().getLebensPunkte() - Spieler1.getSpielerFeuerPokemon().getSpezialAttacke() * 2);
                System.out.println(Spieler2.getSpielerName() + " HP: " + Spieler2.getSpielerPflanzePokemon().getLebensPunkte());
                break;
        }

        System.out.println(Spieler2.getSpielerName() + " ist an der Reihe!");
        System.out.println("1 - Standardattacke");
        System.out.println("2 - Spezialattacke");
        int eingabe8 = scanner.nextInt();

        switch (eingabe8) {
            case 1:
                Spieler1.getSpielerFeuerPokemon().setLebensPunkte(Spieler1.getSpielerFeuerPokemon().getLebensPunkte() - Spieler2.getSpielerPflanzePokemon().getAttackPunkte());
                System.out.println(Spieler1.getSpielerName() + " HP: " + Spieler1.getSpielerFeuerPokemon().getLebensPunkte());
                break;
            case 2:
                Spieler1.getSpielerFeuerPokemon().setLebensPunkte(Spieler1.getSpielerFeuerPokemon().getLebensPunkte() - Spieler2.getSpielerPflanzePokemon().getSpezialAttacke());
                System.out.println(Spieler1.getSpielerName() + " HP: " + Spieler1.getSpielerFeuerPokemon().getLebensPunkte());
                break;
        }
    }

    public void PflanzeGegenFeuer(Spieler Spieler1, Spieler Spieler2) {
        System.out.println(Spieler1.getSpielerName() + " ist an der Reihe!");
        System.out.println("1 - Standardattacke");
        System.out.println("2 - Spezialattacke");
        int eingabe7 = scanner.nextInt();

        switch (eingabe7) {
            case 1:
                Spieler2.getSpielerFeuerPokemon().setLebensPunkte(Spieler2.getSpielerFeuerPokemon().getLebensPunkte() - Spieler1.getSpielerPflanzePokemon().getAttackPunkte());
                System.out.println(Spieler2.getSpielerName() + " HP: " + Spieler2.getSpielerFeuerPokemon().getLebensPunkte());
                break;
            case 2:
                Spieler2.getSpielerFeuerPokemon().setLebensPunkte(Spieler2.getSpielerFeuerPokemon().getLebensPunkte() - Spieler1.getSpielerPflanzePokemon().getSpezialAttacke());
                System.out.println(Spieler2.getSpielerName() + " HP: " + Spieler2.getSpielerFeuerPokemon().getLebensPunkte());
                break;
        }

        System.out.println(Spieler2.getSpielerName() + " ist an der Reihe!");
        System.out.println("1 - Standardattacke");
        System.out.println("2 - Spezialattacke");
        int eingabe8 = scanner.nextInt();

        switch (eingabe8) {
            case 1:
                Spieler1.getSpielerPflanzePokemon().setLebensPunkte(Spieler1.getSpielerPflanzePokemon().getLebensPunkte() - Spieler2.getSpielerFeuerPokemon().getAttackPunkte() * 2);
                System.out.println(Spieler1.getSpielerName() + " HP: " + Spieler1.getSpielerWasserPokemon().getLebensPunkte());
                break;
            case 2:
                Spieler1.getSpielerPflanzePokemon().setLebensPunkte(Spieler1.getSpielerPflanzePokemon().getLebensPunkte() - Spieler2.getSpielerFeuerPokemon().getSpezialAttacke() * 2);
                System.out.println(Spieler1.getSpielerName() + " HP: " + Spieler1.getSpielerWasserPokemon().getLebensPunkte());
                break;
        }
    }

    public void PflanzeGegenWasser(Spieler Spieler1, Spieler Spieler2) {
        System.out.println(Spieler1.getSpielerName() + " ist an der Reihe!");
        System.out.println("1 - Standardattacke");
        System.out.println("2 - Spezialattacke");
        int eingabe7 = scanner.nextInt();

        switch (eingabe7) {
            case 1:
                Spieler2.getSpielerWasserPokemon().setLebensPunkte(Spieler2.getSpielerWasserPokemon().getLebensPunkte() - Spieler1.getSpielerPflanzePokemon().getAttackPunkte() * 2);
                System.out.println(Spieler2.getSpielerName() + " HP: " + Spieler2.getSpielerWasserPokemon().getLebensPunkte());
                break;
            case 2:
                Spieler2.getSpielerWasserPokemon().setLebensPunkte(Spieler2.getSpielerWasserPokemon().getLebensPunkte() - Spieler1.getSpielerPflanzePokemon().getSpezialAttacke() * 2);
                System.out.println(Spieler2.getSpielerName() + " HP: " + Spieler2.getSpielerWasserPokemon().getLebensPunkte());
                break;
        }

        System.out.println(Spieler2.getSpielerName() + " ist an der Reihe!");
        System.out.println("1 - Standardattacke");
        System.out.println("2 - Spezialattacke");
        int eingabe8 = scanner.nextInt();

        switch (eingabe8) {
            case 1:
                Spieler1.getSpielerPflanzePokemon().setLebensPunkte(Spieler1.getSpielerPflanzePokemon().getLebensPunkte() - Spieler2.getSpielerWasserPokemon().getAttackPunkte());
                System.out.println(Spieler1.getSpielerName() + " HP: " + Spieler1.getSpielerPflanzePokemon().getLebensPunkte());
                break;
            case 2:
                Spieler1.getSpielerPflanzePokemon().setLebensPunkte(Spieler1.getSpielerPflanzePokemon().getLebensPunkte() - Spieler2.getSpielerWasserPokemon().getSpezialAttacke());
                System.out.println(Spieler1.getSpielerName() + " HP: " + Spieler1.getSpielerPflanzePokemon().getLebensPunkte());
                break;
        }
    }

    public void WasserGegenPflanze(Spieler Spieler1, Spieler Spieler2) {
        System.out.println(Spieler1.getSpielerName() + " ist an der Reihe!");
        System.out.println("1 - Standardattacke");
        System.out.println("2 - Spezialattacke");
        int eingabe7 = scanner.nextInt();

        switch (eingabe7) {
            case 1:
                Spieler2.getSpielerPflanzePokemon().setLebensPunkte(Spieler2.getSpielerPflanzePokemon().getLebensPunkte() - Spieler1.getSpielerWasserPokemon().getAttackPunkte());
                System.out.println(Spieler2.getSpielerName() + " HP: " + Spieler2.getSpielerPflanzePokemon().getLebensPunkte());
                break;
            case 2:
                Spieler2.getSpielerPflanzePokemon().setLebensPunkte(Spieler2.getSpielerPflanzePokemon().getLebensPunkte() - Spieler1.getSpielerWasserPokemon().getSpezialAttacke());
                System.out.println(Spieler2.getSpielerName() + " HP: " + Spieler2.getSpielerPflanzePokemon().getLebensPunkte());
                break;
        }

        System.out.println(Spieler2.getSpielerName() + " ist an der Reihe!");
        System.out.println("1 - Standardattacke");
        System.out.println("2 - Spezialattacke");
        int eingabe8 = scanner.nextInt();

        switch (eingabe8) {
            case 1:
                Spieler1.getSpielerWasserPokemon().setLebensPunkte(Spieler1.getSpielerWasserPokemon().getLebensPunkte() - Spieler2.getSpielerPflanzePokemon().getAttackPunkte() * 2);
                System.out.println(Spieler1.getSpielerName() + " HP: " + Spieler1.getSpielerWasserPokemon().getLebensPunkte());
                break;
            case 2:
                Spieler1.getSpielerWasserPokemon().setLebensPunkte(Spieler1.getSpielerWasserPokemon().getLebensPunkte() - Spieler2.getSpielerPflanzePokemon().getSpezialAttacke() * 2);
                System.out.println(Spieler1.getSpielerName() + " HP: " + Spieler1.getSpielerWasserPokemon().getLebensPunkte());
                break;
        }
    }
}
